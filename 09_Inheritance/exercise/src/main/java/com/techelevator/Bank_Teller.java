package com.techelevator;

class Bank_Teller {
    public static void main(String argv[]) {
        BankAccount checkingAccount = new CheckingAccount("Bernice", "CHK:1234");
        BankAccount savingsAccount = new SavingsAccount("Bernice", "SAV:9876");

        int amountToDeposit = 2;

        System.out.println("CheckingAccount");
        int newBalance = checkingAccount.deposit(amountToDeposit);
        System.out.println(newBalance);

        newBalance = checkingAccount.withdraw(81);
        System.out.println(newBalance); //new balance is -89 as 10.00 overdraft fee deducted

        newBalance = checkingAccount.withdraw(10);
        System.out.println(newBalance); //new balance is -109 as 10.00 overdraft fee deducted

        newBalance = checkingAccount.withdraw(11);
        System.out.println(newBalance); //balance remains -109 as balance below -100

        newBalance = checkingAccount.deposit(20);
        System.out.println(newBalance); //new balance is -89 as 20.00 deposited

        newBalance = checkingAccount.withdraw(11);
        System.out.println(newBalance); //balance remains -109 as balance goes -100

        System.out.println("SavingsAccount");
        newBalance = savingsAccount.deposit(amountToDeposit);
        System.out.println(newBalance);

        newBalance = savingsAccount.withdraw(2);
        System.out.println(newBalance);
    }
}
