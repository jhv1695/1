package com.techelevator;

public class CreditCardAccount implements Accountable {
    //Accountable account = new CreditCardAccount();
    private String accountHolder;
    private String accountNumber;
    private int debt;
    //Constructor
    public CreditCardAccount(String accountHolder, String accountNumber){
        this.accountHolder = accountHolder;
        this.accountNumber = accountNumber;
        this.debt=0;
    }

    //Getter
    public String getAccountHolder() {
        return accountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getDebt() {
        return debt;
    }


    //Method

    public int pay(int amountToPay) {
        this.debt = this.debt - amountToPay;
        return this.debt;
    }

    public int charge(int amountToCharge) {
        this.debt = this.debt + amountToCharge;
        return this.debt;
    }
    public int getBalance(){
        int balance = 0 - this.debt;
        return balance;
    }

}


