package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer  {
    private String name;
    private String address;
    private String phoneNumber;
    private List<Accountable> accounts = new ArrayList<>();



    //Getter
    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    //Loop
    public Accountable[] getAccounts() {
        Accountable[] accountsArray = new Accountable[accounts.size()];
        for (int index = 0; index < accounts.size(); index++) {
            accountsArray[index] = accounts.get(index);
        }
        return accountsArray;
    }

    public void addAccount(Accountable newAccount) {
        this.accounts.add(newAccount);
    }

    //Setter
    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //isVip
    int totalBalance = 0;
    public boolean isVip() {
        for (Accountable account: accounts) {
            totalBalance = totalBalance + account.getBalance();
        }
        if (totalBalance >= 25_000){
            return true;
        }
        return false;
    }
}


























