package com.techelevator;

import java.util.ArrayList;

public class BankAccount implements Accountable {

    private String accountHolderName;
    private String accountNumber;
    private ArrayList<Accountable> accounts = new ArrayList<>();
    private int balance;

    public BankAccount(String accountHolder, String accountNumber) {
        this.accountHolderName = accountHolder;
        this.accountNumber = accountNumber;
        this.balance = 0;
    }

    public BankAccount(String accountHolder, String accountNumber, int balance) {
        this.accountHolderName = accountHolder;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }


    public int getBalance() {
        return balance;
    }

    public int deposit(int amountToDeposit) {
        balance = balance + amountToDeposit;
        return balance;
    }

    public int withdraw(int amountToWithdraw) {
        balance = balance - amountToWithdraw;
        return balance;
    }
    public int transferTo(BankAccount destinationAccount, int transferAmount) {
        int newBalance = this.withdraw(transferAmount);
        destinationAccount.deposit(transferAmount);

        return newBalance;
    }

    public boolean isVip() {
        int balance = 0;
        for (int i = 0; i < this.accounts.size(); i++) {
            Accountable curAcc = this.accounts.get(i);
            balance = balance + curAcc.getBalance();
        }
        if (balance >= 25000) {
            return true;
        } else {
            return false;
        }
    }
}


